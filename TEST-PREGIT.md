# test-pregit.sh - Introduction

This script implements basic happy day scenario acceptance test for pregit.
For comparison similar for regular git in test-linus-git.sh

## Example test run in a happy sunny day

```
=============================================
This is acceptance test for git init function
  -> test directory will be: .tmp/tdir2294724529
  -> version control dir: .git
=============================================
1) Create new folder. Verify folder created
 ---------------------------------------------
2) Initialize repo. Verify repo directory created.
 ---------------------------------------------
3) Verify directory for objects exists.
 ---------------------------------------------
4) Add a file to repo. Save the commit id for later use. No tests.
 ---------------------------------------------
5) Replace file content, commit. Test initial value vanished.
 ---------------------------------------------
6) Checkout earlier version 08faf12. Verify earlier content
 ---------------------------------------------
7) Checkout back to HEAD. Verify latest value restored.
=============================================
END OF ACCEPTANCE TEST (0)
=============================================
```