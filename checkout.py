#!/usr/bin/python3

import yaml
import hashlib
import os
import pwd
import shutil
from datetime import datetime
from repo import PreFs

class PreCheckout:

    def __init__(self, id='') -> None:        
        self.fs = PreFs()  
        self.tip = ''
        if (not id or id == '-'):
            #print("Checkout <<main tip>>")
            self.commit_id = self.fs.find_main_tip()
            self.tip = '(TIP)'
        else:
            self.commit_id = id

        self.fs.set_commit_id(self.commit_id)              

    def execute(self):
        if (not self.commit_id):
            print("Not commit id available")
            return       

        print ("Checkout to: " + self.commit_id + " " + self.tip)
        if (self.fs.findCommit()):
            # print ("Commit found: " + self.fs.getCommitFile())
            self.commit_data = self.fs.findCommitData()
            self.fs.materialize('tmpxx', self.commit_data)
        else:
            print("\n<< Sorry ... not such commit found: " + self.commit_id + " >>\n")
            


