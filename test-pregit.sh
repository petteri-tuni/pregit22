#!/bin/bash

TESTDIR=.tmp/tdir$RANDOM$RANDOM
STEP=1
SCMDIR=".pregit"
SCMDIR2=".pregit/b"
TESTFILE1=file1.txt

my_error_exit() {
    echo "--- ERROR $1 ---"
    exit $1    
}

echo '============================================='
echo 'This is acceptance test for Pregit 0.1 (init, commit, checkout)'
echo " -> test directory will be: $TESTDIR"
echo " -> version control dir: $SCMDIR"
echo '============================================='

echo '1) Create new folder. Verify folder created'

mkdir -p $TESTDIR
[ ! -d "$TESTDIR" ] && my_error_exit $STEP

echo ' ---------------------------------------------'; STEP=`expr $STEP + 1`
echo '2) Initialize repo. Verify repo directory created.'

cd $TESTDIR
TXTRES=`pregit.py init 2>&1`

[ ! -d "$SCMDIR" ] &&  my_error_exit $STEP  

echo ' ---------------------------------------------'; STEP=`expr $STEP + 1`
echo '3) Verify directory for objects exists.'

[ ! -d "$SCMDIR2" ]  && my_error_exit $STEP

echo ' ---------------------------------------------'; STEP=`expr $STEP + 1`
echo "$STEP) Create new file, add content. Commit to repo. Save commit id for later use. No tests."

TESTVAL1="xyz$RANDOM"
echo $TESTVAL1 > file1.txt
#pregit add file1.txt
TXTRES=`pregit.py commit -m "Initial $TESTVAL1" 2>&1`
COMMIT1=`echo $TXTRES | awk '{print $3}' | sed -e 's/]//g'`

echo ' ---------------------------------------------'; STEP=`expr $STEP + 1`
echo "$STEP) Replace file content, commit. Test initial value vanished."

TESTVAL2="qwe$RANDOM"
echo $TESTVAL2 > file1.txt
TXTRES=`pregit.py commit -m "Replaced with $TESTVAL2" 2>&1`
COMMIT2=`echo $TXTRES | awk '{print $3}' | sed -e 's/]//g'`

# This should be empty:
[[ $(grep $TESTVAL1 file1.txt | wc -c) -ne 0 ]] && my_error_exit $STEP

echo ' ---------------------------------------------'; STEP=`expr $STEP + 1`
echo "$STEP) Checkout earlier version ($COMMIT1). Verify earlier content"

TXTRES=`pregit.py checkout $COMMIT1 2>&1`

# This should found value. No value is error.
[[ $(grep $TESTVAL1 file1.txt | wc -c) -eq 0 ]] && my_error_exit $STEP

echo ' ---------------------------------------------'; STEP=`expr $STEP + 1`
echo "$STEP) Checkout back to latest commit. Verify latest value restored."

#TXTRES=`pregit.py checkout $COMMIT2 2>&1`
TXTRES=`pregit.py checkout - 2>&1`

# This should found value. No value is error.
[[ $(grep $TESTVAL2 file1.txt | wc -c) -eq 0 ]] && my_error_exit $STEP

echo '============================================='
echo "END OF ACCEPTANCE TEST ($?)"
echo '============================================='

