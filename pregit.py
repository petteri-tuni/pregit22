#!/usr/bin/python3

import argparse
import os
import hashlib
from commit import PreCommit
from init import PreInit
from checkout import PreCheckout

#print (hashlib.sha1(b"Nobody inspects the spammish repetition").hexdigest())

description = """
    A General tool to run command line commands.
    Commands are stored as templates to which parameters given in JSON format are filled in.    
    """
parser = argparse.ArgumentParser(description=description)
parser.add_argument("-m", "--msg", default="",  help="Message for information")
parser.add_argument("-a", "--all", default="", help="Command applies to all files")
parser.add_argument("-v", "--verbose", default="0", help="Verbose level")
parser.add_argument('command', nargs='?', default='help', help='PreGit command (init, commit, ...)')
parser.add_argument('prm', nargs='*', help='Parameters')

args = parser.parse_args()
if (args.command == 'help'):
    parser.print_help()    
    exit

cmd = args.command
prm_list = args.prm

if (int(args.verbose) >= 1):
    print ("Command: " + cmd)
    for i in prm_list:
        print ("Parameter: " + i)
    
# -------------------------------------------------

if (cmd == 'init'):
    # print ("Go and create file structure: .pregit, under it HEAD file, objects dir etc...")
    initObj = PreInit()
    initObj.create_repo()  
elif (cmd == 'commit'):
    commitObj = PreCommit();
    commitObj.do_commit()
    # print ("Go and create new version for files given as parameters")    
elif (cmd == 'checkout'):
    checkoutObj = PreCheckout(prm_list[0])
    # print ("Commit id: " + prm_list[0])
    checkoutObj.execute()    
elif (cmd == 'log'):
    pass
else:
    parser.print_help()

