#!/bin/bash

SCMDIR1=".pregit"
SCMDIR2=".git/c"
SCMDIR3=".git/b"

my_error_exit() {
    echo "--- ERROR $1 ---"
    exit $1    
}

echo '============================================='
echo This is acceptance test for pregit init function
echo '============================================='


echo "1) Prepare test setup"

TESTDIR=.tmp/tdir$RANDOM$RANDOM
mkdir -p $TESTDIR

#[ ! -d "$TESTDIR" ] && my_error_exit $STEP

if [ ! -d "$TESTDIR" ]; then 
    my_error_exit 1
fi

echo "2) Test the command exists"

cd $TESTDIR
TXTRES=`xpregit.py 2>&1`
echo "Error status: $?"

# TODO: ADD TEST HERE for $?

echo "3) Test init function. Verify git repo directory is created."

TXTRES=`pregit.py init 2>&1`

[ ! -d "$SCMDIR1" ] &&  my_error_exit 3




