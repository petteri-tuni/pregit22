#!/usr/bin/python3

from repo import PreFs

class PreInit:

    def create_repo(self):
        fsObj = PreFs()

        if (fsObj.check_repo_exists()):
            print ("PGit is initialized already")
        else:
            fsObj.create_repo_skeleton()
            print("Repo skeleton created. Repo dir: " + fsObj.get_repo_dir())