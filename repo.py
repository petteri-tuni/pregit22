import shutil
import os
import pwd
import yaml
import hashlib
from os.path import exists
from datetime import datetime

pdir='.pregit'
commitdir = pdir + '/c'
blobdir = pdir + '/b'
refsdir = pdir + '/refs'
logsdir = pdir + '/logs'
headfile = pdir + '/HEAD'
tipref = refsdir + '/tip'

class PreFs:
    def __init__(self, commit_id='') -> None:
        if (commit_id):
            self.commit_id = commit_id
        pass

    def findCommit(self, commit_id=''):
        if (not commit_id):
            commit_id = self.commit_id
        # print("finding commit id for " + commit_id)

        commit_found = ''
        for path, subdirs, files in os.walk(commitdir):
            for name in files:
                if(commit_id == name):
                    commit_found = os.path.join(path,name)                            

        self.commit_file = commit_found
        return commit_found

    def getCommitFile(self):
        return self.commit_file

    def findCommitData(self):
        commit_data = dict()
        if (self.findCommit(self.commit_id)):            
            with open(self.commit_file, 'r') as file:
                file_content = file.read()
                self.commit_data = yaml.safe_load(file_content)
                #print ("YAML from commit: " + str(self.commit_data))
                commit_data = self.commit_data
        return commit_data

    
    def clearWD(self):
        current_dir = os.getcwd()
        if (len(current_dir) < 10 or current_dir == '/'):
            print ("Current directory too close to root ...")
            exit

        #print ("Current dir: " + os.getcwd())
        #print("CUR DIR: " + str(os.listdir()))     

        wd_dir = '.'
        for path, subdirs, files in os.walk(wd_dir):        
            # print ("SUBDIRS: " + str(subdirs))
            # print ("PATH: " + str(path))            
            if (path == '.'):
                for name in files:                    
                    filepath = os.path.join(wd_dir, name) 
                    # print ("REMOVE FILE: " + filepath)    
                    os.remove(filepath)


    def materialize(self, dir, data):
        blobs = data['blobs']
        item = dict()        
        self.clearWD()
        for item in blobs:            
            filename = item['name']
            blobfile = blobdir + '/' + item['blob']
            # print("Name: " + filename + " - Blobfile: " + blobfile)
            shutil.copyfile(blobfile, filename)
            

    def check_repo_exists(self):            
        if (exists(pdir)):
            return True
        else:
            return False           
           
    def create_repo_skeleton(self):
        os.mkdir(pdir)
        os.mkdir(blobdir)
        os.mkdir(refsdir)
        os.mkdir(commitdir)     

    def get_repo_dir(self):
        return pdir   

    def write_commit_file(self, commit_hash, yaml_data):
        # ------------------------------------------------
        # Write commit data
        
        filename = commitdir + '/' + commit_hash

        with open(filename, 'w') as file:
            file.write(yaml_data)
        self.write_head(commit_hash)        

    def write_blob_file(self, filename, hashval):
        blobfile = blobdir + '/' + hashval
        shutil.copyfile(filename, blobfile)

        #with open(filename, 'w') as file:
        #   file.write(file_content)        

    def write_head(self, commit_hash):
        with open(tipref, 'w') as file:
            file.write(commit_hash)                 
        with open(headfile, 'w') as file:
            file.write(tipref)   

    def set_commit_id(self, commit_id):
        self.commit_id = commit_id

    def find_main_tip(self):        
        tip_commit = ''
        with open(tipref, 'r') as file:
            tip_commit = file.read()

        return tip_commit
