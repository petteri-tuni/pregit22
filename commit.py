#!/usr/bin/python3

import yaml
import hashlib
import os
import pwd
import shutil
from datetime import datetime
from repo import PreFs

class PreCommit:
    def __init__(self) -> None:
        self.fs_obj = PreFs()       

    def calc_file_hash(self, filename):
        with open(filename, 'r') as file:
            file_content = file.read()

        return hashlib.sha1(bytes(file_content,'utf-8')).hexdigest()    

    def save_blob(self, filename, hashval=0):
        # ------------------------------------------------
        # Write individual blob/file data
        if (hashval == 0):
            hashval = self.calc_file_hash(filename)

        self.fs_obj.write_blob_file(filename, hashval)                

    def save_head(self, commit_hash):            
        self.fs_obj.write_head(commit_hash)          

    def save_commit(self, yaml_data):
        # ------------------------------------------------
        # Write commit data

        self.commit_hash = hashlib.sha1(bytes(yaml_data,'utf-8')).hexdigest()
        self.fs_obj.write_commit_file(self.commit_hash, yaml_data)

    # ------------------------------------------------

    def do_commit(self):    
        #filelist = ['dataoper.py', 'pregit.py']
        filelist = os.listdir()

        bloblist = []
        for tmp in filelist:
            if (tmp != '.pregit'):
                hashval = self.calc_file_hash(tmp)
                bloblist.append(dict(name=tmp, blob=hashval))
                self.save_blob(tmp,hashval)

        # Getting the current date and time
        dt = datetime.now()

        data2 = dict()
        data2['author'] = pwd.getpwuid(os.getuid()).pw_name        
        data2['timestamp']= dt
        data2['blobs'] = bloblist

        yaml_data = yaml.safe_dump(data2)

        self.save_commit(yaml_data)
        print("Commit id: "+self.commit_hash)
        print (str(yaml_data))
        





